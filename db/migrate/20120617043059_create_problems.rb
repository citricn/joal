class CreateProblems < ActiveRecord::Migration
  def change
    create_table :problems do |t|
      t.string :name
      t.string :code
      t.string :description
      t.string :input
      t.string :output
      t.string :test_case

      t.timestamps
    end
  end
end
