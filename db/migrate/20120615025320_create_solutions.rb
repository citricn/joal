class CreateSolutions < ActiveRecord::Migration
  def change
    create_table :solutions do |t|
      t.string :status

      t.timestamps
    end
  end
end
