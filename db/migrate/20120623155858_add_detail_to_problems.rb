class AddDetailToProblems < ActiveRecord::Migration
  def change
    add_column :problems, :test_case_out, :string
    rename_column :problems, :test_case, :test_case_in

  end
end
