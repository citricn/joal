class AddPathToSolution < ActiveRecord::Migration
  def change
    add_column :solutions, :path, :string
  end
end
