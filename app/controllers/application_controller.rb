class ApplicationController < ActionController::Base
  protect_from_forgery
  helper :all
  helper_method :current_user
  before_filter :require_login
  before_filter :authorization
  helper_method :current_user_session, :current_user

#  private
  def require_login
    unless logged_in?
        flash[:notice] = "You must be logged in to do this"
        redirect_to :login
    end
  end  

  def logged_in?
    !!current_user
  end

  def authorization
    unless current_user.user_group == 'administrator'
        flash[:notice] = " As a competitor you may not access this space"
        redirect_to :root 
    end
  end

  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end
  
  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.record
  end

  def save_file(name, up_file)
     directory = "public/data"
     if up_file != nil
	path = File.join(directory, name)
	File.open(path, "wb") {|f| f.write(up_file.read)}
     end
	return path
  end

  def delete_file(name)
    path = "public/data/" + name
    File.delete(path) if File.exist?(path)
  end

end
