class HomeController < ApplicationController
 skip_before_filter :require_login, :only => [:index]
 skip_before_filter :authorization, :only => [:index]
 skip_before_filter :edit_auth

  def index
    respond_to do |format| 
      format.html
    end
  end
end
