class SolutionsController < ApplicationController

  skip_before_filter :authorization 
  skip_before_filter :edit_auth

  include ApplicationHelper 

  # GET /solutions
  # GET /solutions.json
  def index
    if params[:problem_id]
	@problem      = Problem.find(params[:problem_id])
	@solutions    = @problem.solutions

        respond_to do |format|
           format.html # index.html.erb
           format.json {render json: @solutions, json: @problem}
        end
    elsif params[:user_id]
        @user       = User.find(params[:user_id])
        @solutions  = @user.solutions
 
        respond_to do |format|
           format.html # index.html.erb
           format.json { render json: @solutions,json:@user }
        end
    end
  end 

  # GET /solutions/1
  # GET /solutions/1.json
  def show
    @solution  = Solution.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @solution}
    end
  end

  # GET /solutions/new
  # GET /solutions/new.json
  def new
    @problem  = Problem.find(params[:problem_id])
    @solution = @problem.solutions.build
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @solution }
    end
  end

  # GET /solutions/1/edit
  def edit
    @solution = Solution.find(params[:id])
  end

  # POST /solutions
  # POST /solutions.json
  def create
      @problem_user  = Problem.find(params[:problem_id])
      @solution = @problem_user.solutions.create(:status => params[:solution][:status],:path => "")
	@solution.user = current_user
	#####CODIGO PARA VERIFICAR SI LA SOLUCION ES CORRECTA######
	###ARCHIVOS DE PRUEBA###

	
	directory    = "public/solutions"
	id           = @solution.user.username
	path_to_save = files_save(id,params,directory)
	@solution.path=path_to_save
	test_case_in_path      = @problem_user.test_case_in
	test_case_out_path     = @problem_user.test_case_out
	status       = run_solution(path_to_save,test_case_in_path,test_case_out_path,"ruby")
	@solution.status = status

	##########################################################
    respond_to do |format|
      if @solution.save
        format.html { redirect_to problem_solutions_path, notice: 'Solution was successfully created.' }
        format.json { render json: @solution, status: :created, location: @solution }
      else
        format.html { render action: "new" }
        format.json { render json: @solution.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /solutions/1
  # PUT /solutions/1.json
  def update
    @solution = Solution.find(params[:id])

    respond_to do |format|
      if @solution.update_attributes(params[:solution])
        format.html { redirect_to problem_solutions_path, notice: 'Solution was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @solution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /solutions/1
  # DELETE /solutions/1.json
  def destroy
    @solution = Solution.find(params[:id])
    delete_file(@solution.path)
    @solution.destroy

    respond_to do |format|
      format.html { redirect_to problem_solutions_path }
      format.json { head :no_content }
	  format.js
    end
  end

  def is_marked(problem)
    res = false
    if problem.solutions.size > 0
      problem.solutions.each do |solution|
        if solution.user == current_user
          res = true
          break
        end
      end
    end
    return res
  end

   def countsol(user)
    res = 0
    if user.solutions.size > 0
      user.solutions.each do |solution|
        if solution.status != 'not tried'
          res = res + 1
        end
      end
    end
    return res
  end


  end
