class ProblemsController < ApplicationController
  skip_before_filter :authorization, :only => [:show, :index, :mark, :top_users, :problem_solved]
  skip_before_filter :require_login, :only => [:show, :index]
  skip_before_filter :edit_auth


  # Find the top users of a particular problem
  def top_users
    @problem = Problem.find(params[:id])
    @solutions = @problem.solutions.where(:status => "correct")
    
    respond_to do |format|
	format.html # top_users.html.erb
        format.json {render json: @problem}

    end
  end

  def mark
    @problems = Problem.all
    @problem = Problem.find(params[:id])
    @solution = Solution.create(:status => "not tried",:path => "")
    @solution.problem = @problem
    @solution.user = current_user

    if @solution.save
      respond_to do |format|
        format.json { render json: @problem , notice: 'Problem was successfully marked. Now you can resolve it!'}
        format.js
      end
    else
      format.json { render error: "error trying to mark" }
      
    end
  end
## Basic Methods ##

  # GET /problems
  # GET /problems.json
  def index
    @problems = Problem.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @problems }
    end
  end

  # GET /problems/1
  # GET /problems/1.json
  def show
    @problem = Problem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @problem }
    end
  end

  # GET /problems/new
  # GET /problems/new.json
  def new
    @problem = Problem.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @problem }
    end
  end

  # GET /problems/1/edit
  def edit
    @problem = Problem.find(params[:id])
  end

  # POST /problems
  # POST /problems.json
  def create
    @problem = Problem.new(:code => params[:problem][:code], :name => params[:problem][:name], :description => params[:problem][:description], :input => params[:problem][:input], :output => params[:problem][:output], :test_case_in => " ", :test_case_out => " ")
   
    # Save the uploaded files in public/data
    name_in = "in-" + @problem.code
    name_out = "out-" + @problem.code
    path_in  = save_file(name_in, params[:problem][:file_in])
    path_out = save_file(name_out, params[:problem][:file_out]) 

    @problem.test_case_in = path_in
    @problem.test_case_out = path_out

    respond_to do |format|
      if @problem.save
        format.html { redirect_to @problem, notice: 'Problem was successfully created.' }
        format.json { render json: @problem, status: :created, location: @problem }
      else
        format.html { render action: "new" }
        format.json { render json: @problem.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /problems/1
  # PUT /problems/1.json
  def update
    @problem = Problem.where(:code => params[:problem][:code]).first

    # Save the new files
    name_in = "in-" + @problem.code
    name_out = "out-" + @problem.code 
    path_in=save_file(name_in, params[:problem][:file_in])
    path_out=save_file(name_out, params[:problem][:file_out])

    @problem.test_case_in  = path_in
    @problem.test_case_out = path_out


    respond_to do |format|
      if @problem.update_attributes(:code => params[:problem][:code], :name => params[:problem][:name], :description => params[:problem][:description], :input => params[:problem][:input], :output => params[:problem][:output])
        format.html { redirect_to @problem, notice: 'Problem was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @problem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /problems/1
  # DELETE /problems/1.json
  def destroy
    @problem = Problem.find(params[:id])
    solutions = @problem.solutions
    solutions.each do |solution|
      solution.destroy
    end
    
    # Delete test case files
    delete_file(@problem.test_case_in)
    delete_file(@problem.test_case_out)   
   
    @problem.destroy
    

    respond_to do |format|
      format.html { redirect_to problems_url }
      format.json { head :no_content }
	  format.js
    end
  end

  
  def is_marked(problem)
    res = false
    if problem.solutions.size > 0
      problem.solutions.each do |solution|
        if solution.user == current_user
          res = true
          break
        end
      end
    end
    return res
  end

  def countsol(problem)
    res = 0
    if problem.solutions.size > 0
      problem.solutions.each do |solution|
        if solution.status != 'not tried'
          res = res + 1
        end
      end
    end
    return res
  end

  def problem_solved(problem)
    res = false

    if problem.solutions.size > 0
       problem.solutions.each do |solution|
          if solution.user == current_user && solution.status == "correct"
	     res = true
             break
          end
       end
    end
    
    return res
  end
  
end
