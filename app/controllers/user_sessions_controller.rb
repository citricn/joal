class UserSessionsController < ApplicationController
  skip_before_filter :require_login
  skip_before_filter :authorization
  skip_before_filter :edit_auth

  # GET /user_sessions.json
  # GET /user_sessions/new
  # GET /user_sessions/new.json
  def new
    @user_session = UserSession.new

    respond_to do |format|
        format.html # new.html.erb
        format.xml  { render :xml => @user_session }
    end
  end


  # POST /user_sessions
  # POST /user_sessions.json
  def create
    @user_session = UserSession.new(params[:user_session])

    respond_to do |format|
      if @user_session.save
        format.html { redirect_to(:problems, :notice => 'Login Successfull') }
        format.json { render json: @user_session, status: :created, location: @user_session }
      else
        format.html { render action: "new" }
        format.json { render json: @user_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /user_sessions/1
  # PUT /user_sessions/1.json

  # DELETE /user_sessions/1
  # DELETE /user_sessions/1.json
  def destroy
    @user_session = UserSession.find(params[:id])
    @user_session.destroy

    respond_to do |format|
        flash[:notice] = 'Goodbye.'
        format.html { redirect_to(:controller => 'home') }
        #format.html redirect_to :home => 'home#index', :notice => 'ssd'
        #format.html { redirect_to(:home, :notice => 'Goodbye!') }
        #format.xml  { head :ok }
    end
  end

end
