class UsersController < ApplicationController
 #before_filter :authenticate, :only => [:index, :edit, :show, :update, :destroy]
 #before_filter :check_auth, :only => [:edit, :update, :destroy]
 skip_before_filter :require_login, :only => [:new, :create]
 skip_before_filter :authorization, :only => [:new, :create, :index, :show, :edit, :update]
 before_filter :edit_auth, :only => [:edit, :update, :destroy]

  def edit_auth
    unless current_user.username == User.find(params[:id]).username
      flash[:notice] = "You may not edit this object"
      if request.referer != nil
        redirect_to request.referer 
      else
        redirect_to :root
      end
        
    end    
  end

## Basic Methods ##

  # GET /users
  # GET /users.json
  def index
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])
    @user.user_group = "competitor"

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  
  def countsol(user)
    res = 0
    if user.solutions.size > 0
      user.solutions.each do |solution|
        if solution.status != 'not tried'
          res = res + 1
        end
      end
    end
    return res
  end


end
