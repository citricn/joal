module ApplicationHelper


  def files_save(id,params,path)
    directory    = path
	 
	name_in = "solution-" + id + Time.now.to_s.delete(' ') + ".rb"
    
    save_path = File.join(directory, name_in)

    if params[:solution][:path] then
       File.open(save_path, "wb") {|f| f.write(params[:solution][:path].read)}
    end
	return save_path
  end

  def files_delete(solution,path)
    solution_path = path+"solution" + solution.id

    File.delete(solution_path) if File.exists?(solution_path)
  end

def dump_file(file_path)
	solution_file = File.open(file_path,'rb')
	solution = solution_file.readlines
	solution_string = ""
	solution.each do | line |
		solution_string = solution_string << line
	end
	return solution_string
end

def exec_command(command)
	output = %x[#{command}]
	return output
end

def compare_solution(solution,real_solution_path)
	string_solution = dump_file(real_solution_path)
	string_solution = string_solution.chomp
	solution        = solution.chomp
	return solution.eql?(string_solution)
end


def run_solution(program_file_path,input_file_path,real_solution_path,lenguage)
	status   = "wrong"
	commands = Hash.new
	commands['ruby']   = "ruby #{program_file_path}"
	commands['python'] = "python #{program_file_path}"
	command = commands[lenguage] << " < " << input_file_path

	print "COMMANNNDO" + command
	pre_run = system(command << " 2>/dev/null")
	if pre_run then	
		output  = exec_command(command)
		is_correct = compare_solution(output,real_solution_path)
		if is_correct then
			status = "correct"
		else
			status = "incorrect"
		end
	else
		status = "error"
	end
	return status
end



end
