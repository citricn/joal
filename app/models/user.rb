class User < ActiveRecord::Base


  acts_as_authentic
  attr_accessible :password, :password_confirmation,:crypted_password, :email, :password_salt, :persistence_token, :type, :username,:user_group, :solutions,:problems
  has_many:solutions
  has_many:problems, :through => :solutions

  validates :user_group, :inclusion => { :in => ["administrator", "competitor"]}
  
  
end
