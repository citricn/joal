class Problem < ActiveRecord::Base
  attr_accessible :code, :description, :input, :name, :output, :test_case_in, :test_case_out
  validates :name,          :presence => true
  validates :description,   :presence => true
  validates :code,          :presence => true,
  				  :length   => {:is =>5},
                                  :uniqueness => true
  validates :description,   :presence => true
  validates :input,         :presence => true
  validates :output,        :presence => true
  validates :test_case_in,  :presence => true
  validates :test_case_out, :presence => true
  has_many:solutions
  has_many:users, :through => :solutions
end
