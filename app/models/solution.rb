class Solution < ActiveRecord::Base
  attr_accessible :status,:path 
  belongs_to:user
  attr_accessible :status,:path,:user,:problem
  validates :status, :presence => true 
  belongs_to:user,:autosave => true
  belongs_to:problem

end
